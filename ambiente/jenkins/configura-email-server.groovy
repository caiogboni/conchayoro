import jenkins.model.*

def inst = Jenkins.getInstance()

def desc = inst.getDescriptor("hudson.tasks.Mailer")

desc.setSmtpHost(System.getenv('SMARTHOST_ADDRESS'))
desc.setSmtpPort(System.getenv('SMARTHOST_PORT'))
desc.setSmtpAuth(System.getenv('SMARTHOST_USER'), System.getenv('SMARTHOST_PASSWORD'))
desc.setUseSsl(false)
desc.setCharset("UTF-8")
desc.setReplyToAddress(System.getenv('SMARTHOST_REPLY_ADDRESS'))

desc.save()

